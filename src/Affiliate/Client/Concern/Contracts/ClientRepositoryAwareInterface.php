<?php
namespace Innovation\Affiliate\Client\Concern\Contracts;


/**
 * Interface ClientRepositoryAwareInterface
 * @package Innovation\Affiliate\Client\Contracts
 */
interface ClientRepositoryAwareInterface
{
    /**
     * Set client repository.
     *
     * @param ClientRepositoryInterface $clientRepository
     */
    public function setClientRepository(ClientRepositoryInterface $clientRepository);
}