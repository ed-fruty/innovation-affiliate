<?php
namespace Innovation\Affiliate\Client\Concern\Contracts;

use Carbon\Carbon;
use Innovation\Affiliate\Client\Concern\ValueObjects\ClientId;
use Innovation\Affiliate\Client\Concern\ValueObjects\ProjectClientId;
use Innovation\Affiliate\Project\Contracts\ProjectInterface;


/**
 * Interface ClientInterface
 * @package Innovation\Affiliate\Client\Contracts
 */
interface ClientInterface
{
    /**
     * Get client id.
     *
     * @return ClientId
     */
    public function getId() : ClientId;

    /**
     * Get client project.
     *
     * @return ProjectInterface
     */
    public function getProject() : ProjectInterface;

    /**
     * Get client project internal id.
     *
     * @return ProjectClientId
     */
    public function getProjectClientId() : ProjectClientId;

    /**
     * Get client registration date.
     *
     * @return Carbon
     */
    public function getRegistrationDate() : Carbon;

    /**
     * Check is client is a lead.
     *
     * @return bool
     */
    public function isLead() : bool;
}