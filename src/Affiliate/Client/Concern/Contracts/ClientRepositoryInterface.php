<?php
namespace Innovation\Affiliate\Client\Concern\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Innovation\Affiliate\Client\ValueObjects\ClientId;
use Innovation\Affiliate\Common\Database\Specifications\SpecificationInterface;

/**
 * Interface ClientRepositoryInterface
 * @package Innovation\Affiliate\Client\Contracts
 */
interface ClientRepositoryInterface
{
    /**
     * Create a new instance of client with the given attributes.
     *
     * @param array $attributes
     * @return ClientInterface
     */
    public function newInstance(array $attributes = []) : ClientInterface;

    /**
     * Save client statement.
     *
     * @param ClientInterface $client
     * @return bool
     */
    public function save(ClientInterface $client) : bool;

    /**
     * Find clients by given specifications.
     *
     * @param SpecificationInterface $specification
     * @return Collection
     */
    public function match(SpecificationInterface $specification);

    /**
     * Find client by given client id.
     *
     * @param ClientId $clientId
     * @return ClientInterface|null
     */
    public function findById(ClientId $clientId);
}