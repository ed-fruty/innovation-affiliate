<?php
namespace Innovation\Affiliate\Client\Concern\Commands;

use Innovation\Affiliate\Client\Concern\ValueObjects\ProjectClientId;
use Innovation\Affiliate\Project\Contracts\ProjectInterface;


/**
 * Class CreateClientCommand
 * @package Innovation\Affiliate\Client\Concern\Commands
 */
final class CreateClientCommand
{
    /**
     * @var ProjectInterface
     */
    private $project;
    /**
     * @var ProjectClientId
     */
    private $projectClientId;

    /**
     * CreateClientCommand constructor.
     * @param ProjectInterface $project
     * @param ProjectClientId $projectClientId
     */
    public function __construct(ProjectInterface $project, ProjectClientId $projectClientId)
    {
        $this->project = $project;
        $this->projectClientId = $projectClientId;
    }

    /**
     * @return ProjectInterface
     */
    public function getProject(): ProjectInterface
    {
        return $this->project;
    }

    /**
     * @return ProjectClientId
     */
    public function getProjectClientId(): ProjectClientId
    {
        return $this->projectClientId;
    }
}