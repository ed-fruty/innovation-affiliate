<?php
namespace Innovation\Affiliate\Client\Concern\Traits\Aware;

use Innovation\Affiliate\Client\Concern\Contracts\ClientRepositoryInterface;

/**
 * Trait ClientRepositoryAware
 * @package Innovation\Affiliate\Client\Traits\Aware
 */
trait ClientRepositoryAware
{
    /**
     * @var ClientRepositoryInterface
     */
    protected $clientRepository;

    /**
     * Set client repository.
     *
     * @param ClientRepositoryInterface $clientRepository
     */
    public function setClientRepository(ClientRepositoryInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }
}