<?php
namespace Innovation\Affiliate\Client\Concern\ValueObjects;

use Innovation\Affiliate\Common\Traits\ObjectId;

/**
 * Class ProjectClientId
 * @package Innovation\Affiliate\Client\ValueObjects
 */
final class ProjectClientId
{
    use ObjectId;
}