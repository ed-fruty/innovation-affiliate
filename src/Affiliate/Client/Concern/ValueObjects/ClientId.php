<?php
namespace Innovation\Affiliate\Client\Concern\ValueObjects;

use Innovation\Affiliate\Common\Traits\ObjectId;

/**
 * Class ClientId
 * @package Innovation\Affiliate\Client\ValueObjects
 */
final class ClientId
{
    use ObjectId;
}