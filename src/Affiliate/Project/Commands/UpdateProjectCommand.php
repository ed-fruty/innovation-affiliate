<?php
namespace Innovation\Affiliate\Project\Commands;

use Innovation\Affiliate\Project\ValueObjects\ProjectId;

final class UpdateProjectCommand
{
    /**
     * @var ProjectId
     */
    private $projectId;
    /**
     * @var string
     */
    private $newName;

    /**
     * UpdateProjectCommand constructor.
     * @param ProjectId $projectId
     * @param string $newName
     */
    public function __construct(ProjectId $projectId, string $newName)
    {
        $this->projectId = $projectId;
        $this->newName = $newName;
    }

    /**
     * @return ProjectId
     */
    public function getProjectId(): ProjectId
    {
        return $this->projectId;
    }

    /**
     * @return string
     */
    public function getNewName(): string
    {
        return $this->newName;
    }
}
