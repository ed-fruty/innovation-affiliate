<?php
namespace Innovation\Affiliate\Project\Commands;

/**
 * Class StoreProjectCommand
 * @package Innovation\Affiliate\Project\Commands
 */
final class StoreProjectCommand
{
    /**
     * @var string
     */
    private $name;

    /**
     * StoreProjectCommand constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return [
            'name'  => $this->getName()
        ];
    }

}