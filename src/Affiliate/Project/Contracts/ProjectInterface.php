<?php
namespace Innovation\Affiliate\Project\Contracts;

use Innovation\Affiliate\Project\ValueObjects\ProjectId;

interface ProjectInterface
{

    /**
     * @return ProjectId
     */
    public function getId() : ProjectId;

    /**
     * @return string
     */
    public function getName() : string;
}
