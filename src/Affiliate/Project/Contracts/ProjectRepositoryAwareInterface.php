<?php
namespace Innovation\Affiliate\Project\Contracts;


/**
 * Interface ProjectRepositoryAwareInterface
 * @package Innovation\Affiliate\Project\Contracts
 */
interface ProjectRepositoryAwareInterface
{
    /**
     * Set project repository.
     *
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function setProjectRepository(ProjectRepositoryInterface $projectRepository);
}