<?php
namespace Innovation\Affiliate\Project\Contracts;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;
use Innovation\Affiliate\Common\Database\Specifications\SpecificationInterface;
use Innovation\Affiliate\Project\ValueObjects\ProjectId;

/**
 * Interface ProjectRepositoryInterface
 * @package Innovation\Affiliate\Project\Contracts
 */
interface ProjectRepositoryInterface
{
    /**
     * Find project by given id.
     *
     * @param ProjectId $id
     * @return ProjectInterface|null
     */
    public function findById(ProjectId $id);

    /**
     *
     * @param SpecificationInterface $specification
     * @return Collection|Paginator
     */
    public function match(SpecificationInterface $specification);

    /**
     * Save project statement.
     *
     * @param ProjectInterface $project
     * @return bool
     */
    public function save(ProjectInterface $project);

    /**
     * Create a new instance.
     *
     * @param array $attributes
     * @return ProjectInterface
     */
    public function newInstance(array $attributes = []) : ProjectInterface;
}
