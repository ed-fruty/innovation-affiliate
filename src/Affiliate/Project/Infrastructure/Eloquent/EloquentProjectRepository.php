<?php
namespace Innovation\Affiliate\Project\Infrastructure\Eloquent;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;
use Innovation\Affiliate\Common\Database\Specifications\SpecificationInterface;
use Innovation\Affiliate\Project\Contracts\ProjectInterface;
use Innovation\Affiliate\Project\Contracts\ProjectRepositoryInterface;
use Innovation\Affiliate\Project\ValueObjects\ProjectId;


/**
 * Class EloquentProjectRepository
 * @package Innovation\Affiliate\Project\Infrastructure\Eloquent
 */
class EloquentProjectRepository implements ProjectRepositoryInterface
{
    /**
     * Find project by given id.
     *
     * @param ProjectId $id
     * @return ProjectInterface|null
     */
    public function findById(ProjectId $id)
    {
        // TODO: Implement findById() method.
    }

    /**
     *
     * @param SpecificationInterface $specification
     * @return Collection|Paginator
     */
    public function match(SpecificationInterface $specification)
    {
        // TODO: Implement match() method.
    }

    /**
     * Save project statement.
     *
     * @param ProjectInterface $project
     * @return bool
     */
    public function save(ProjectInterface $project)
    {
        // TODO: Implement save() method.
    }

    /**
     * Create a new instance.
     *
     * @param array $attributes
     * @return ProjectInterface
     */
    public function newInstance(array $attributes = []): ProjectInterface
    {
        // TODO: Implement newInstance() method.
    }
}