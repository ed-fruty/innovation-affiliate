<?php
namespace Innovation\Affiliate\Project\Infrastructure\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Innovation\Affiliate\Project\Contracts\ProjectInterface;
use Innovation\Affiliate\Project\ValueObjects\ProjectId;

final class Project extends Model implements ProjectInterface
{
    private const ATTRIBUTE_ID = 'id';
    private const ATTRIBUTE_NAME = 'name';

    protected const ATTRIBUTES_LIST = [
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_NAME
    ];

    /**
     * @return ProjectId
     */
    public function getId(): ProjectId
    {
        return new ProjectId($this->getAttribute(self::ATTRIBUTE_ID));
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getAttribute(self::ATTRIBUTE_NAME);
    }
}
