<?php
namespace Innovation\Affiliate\Project\Events;

use Innovation\Affiliate\Project\Contracts\ProjectInterface;

final class ProjectWasUpdated
{
    /**
     * @var ProjectInterface
     */
    private $project;
    /**
     * @var array
     */
    private $oldAttributes;

    /**
     * ProjectWasUpdated constructor.
     * @param ProjectInterface $project
     * @param array $oldAttributes
     */
    public function __construct(ProjectInterface $project, array $oldAttributes = [])
    {
        $this->project = $project;
        $this->oldAttributes = $oldAttributes;
    }

    /**
     * @return array
     */
    public function getOldAttributes(): array
    {
        return $this->oldAttributes;
    }

    /**
     * @return ProjectInterface
     */
    public function getProject(): ProjectInterface
    {
        return $this->project;
    }
}
