<?php
namespace Innovation\Affiliate\Project\Events;

use Innovation\Affiliate\Project\Contracts\ProjectInterface;

/**
 * Class ProjectWasCreated
 * @package Innovation\Affiliate\Project\Concern\Events
 */
final class ProjectWasCreated
{
    /**
     * @var ProjectInterface
     */
    private $project;

    /**
     * ProjectWasCreated constructor.
     * @param ProjectInterface $project
     */
    public function __construct(ProjectInterface $project)
    {
        $this->project = $project;
    }

    /**
     * @return ProjectInterface
     */
    public function getProject(): ProjectInterface
    {
        return $this->project;
    }
}
