<?php
namespace Innovation\Affiliate\Project\Listeners;

use Innovation\Affiliate\Common\Events\Routing\BackendRoutesWasInitialized;
use Innovation\Affiliate\Project\Routing\ProjectRouting;

class DefineProjectBackendRoutesListener
{
    /**
     * @var ProjectRouting
     */
    private $projectRouting;

    /**
     * DefineProjectBackendRoutesListener constructor.
     * @param ProjectRouting $projectRouting
     */
    public function __construct(ProjectRouting $projectRouting)
    {
        $this->projectRouting = $projectRouting;
    }

    /**
     * @param BackendRoutesWasInitialized $event
     */
    public function listen(BackendRoutesWasInitialized $event)
    {
        $this->projectRouting->backend($event->getRouter());
    }
}
