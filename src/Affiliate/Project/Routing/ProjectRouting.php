<?php
namespace Innovation\Affiliate\Project\Routing;

use Illuminate\Routing\Router;
use Innovation\Affiliate\Project\Actions\Backend\GetProjectsList\GetProjectsListAction;

/**
 * Class ProjectRouting
 * @package Innovation\Affiliate\Project\Routing
 */
class ProjectRouting
{
    /**
     * @param Router $router
     */
    public function backend(Router $router)
    {
        $router->get('projects')
            ->uses(GetProjectsListAction::class)
            ->name('projects.backend.index');
    }
}
