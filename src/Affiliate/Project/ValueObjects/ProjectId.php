<?php
namespace Innovation\Affiliate\Project\ValueObjects;

/**
 * Class ProjectId
 * @package Innovation\Affiliate\Project\ValueObjects
 */
final class ProjectId
{
    /**
     * @var string
     */
    private $id;

    /**
     * ProjectId constructor.
     * @param $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId() : string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return (string) $this->getId();
    }

    /**
     * @param ProjectId $id
     * @return bool
     */
    public function equals(ProjectId $id) : bool
    {
        return $this->id === $id->getId();
    }
}
