<?php
namespace Innovation\Affiliate\Project\Providers;

use Illuminate\Support\ServiceProvider;
use Innovation\Affiliate\Common\Events\Routing\BackendRoutesWasInitialized;
use Innovation\Affiliate\Common\Providers\Traits\RegisterCommandHandlersProvider;
use Innovation\Affiliate\Common\Providers\Traits\RegisterEventsProvider;
use Innovation\Affiliate\Project\Commands\StoreProjectCommand;
use Innovation\Affiliate\Project\Contracts\ProjectRepositoryAwareInterface as ProjectRepositoryAware;
use Innovation\Affiliate\Project\Contracts\ProjectRepositoryInterface;
use Innovation\Affiliate\Project\Handlers\StoreProjectHandler;
use Innovation\Affiliate\Project\Infrastructure\Eloquent\EloquentProjectRepository;
use Innovation\Affiliate\Project\Listeners\DefineProjectBackendRoutesListener;

/**
 * Class ProjectServiceProvider
 * @package Innovation\Affiliate\Project\Providers
 */
final class ProjectServiceProvider extends ServiceProvider
{
    use RegisterEventsProvider, RegisterCommandHandlersProvider;

    /**
     * @var array
     */
    private const LISTEN = [
        BackendRoutesWasInitialized::class => DefineProjectBackendRoutesListener::class
    ];

    /**
     * @var array
     */
    private const COMMANDS = [
        StoreProjectCommand::class => StoreProjectHandler::class
    ];

    /**
     * List of provides services.
     *
     * @return array
     */
    public function provides()
    {
        return [
            ProjectRepositoryInterface::class
        ];
    }

    /**
     * Fired on application boots.
     */
    public function boot()
    {
        $this->registerEventListeners(self::LISTEN);
        $this->registerCommandHandlers(self::COMMANDS);
        $this->registerProjectRepositoryAware();
    }

    /**
     * Register provided services.
     */
    public function register()
    {
        $this->app->bind(ProjectRepositoryInterface::class, EloquentProjectRepository::class);
    }


    private function registerProjectRepositoryAware()
    {
        $this->app->afterResolving(ProjectRepositoryAware::class, function(ProjectRepositoryAware $aware) {
            $aware->setProjectRepository($this->app[ProjectRepositoryInterface::class]);
        });
    }
}
