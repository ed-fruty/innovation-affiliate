<?php
namespace Innovation\Affiliate\Project\Actions\Backend\UpdateProject;

use Innovation\Affiliate\Common\Aware\Traits\CommandBusAware;
use Innovation\Affiliate\Project\Commands\UpdateProjectCommand;
use Innovation\Affiliate\Project\ValueObjects\ProjectId;

final class UpdateProjectAction
{
    use CommandBusAware;

    /**
     * @param UpdateProjectRequest $request
     * @param UpdateProjectResponder $responder
     * @param $projectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(UpdateProjectRequest $request, UpdateProjectResponder $responder, $projectId)
    {
        $command = new UpdateProjectCommand(new ProjectId($projectId), $request->get('name'));

        $project = $this->commandBus->dispatch($command);

        return $responder->getResponse($project);
    }
}
