<?php
namespace Innovation\Affiliate\Project\Actions\Backend\UpdateProject;

use Illuminate\Http\JsonResponse;
use Innovation\Affiliate\Project\Contracts\ProjectInterface;

class UpdateProjectResponder
{
    /**
     * @param ProjectInterface $project
     * @return JsonResponse
     */
    public function getResponse(ProjectInterface $project)
    {
        return new JsonResponse([
            'id'    => $project->getId(),
            'name'  => $project->getName()
        ]);
    }
}
