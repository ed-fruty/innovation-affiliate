<?php
namespace Innovation\Affiliate\Project\Actions\Backend\CreateProject;

use Illuminate\Http\Response;
use Innovation\Affiliate\Common\Aware\Contracts\ViewFactoryAwareInterface;
use Innovation\Affiliate\Common\Aware\Traits\ViewFactoryAware;

/**
 * Class CreateProjectResponder
 * @package Innovation\Affiliate\Project\Actions\Backend\CreateProject
 */
class CreateProjectResponder implements ViewFactoryAwareInterface
{
    use ViewFactoryAware;

    private const VIEW_NAME = 'projects.backend.create';

    /**
     * @return Response
     */
    public function getResponse()
    {
        return new Response(
            $this->viewFactory->make(self::VIEW_NAME)
        );
    }
}
