<?php
namespace Innovation\Affiliate\Project\Actions\Backend\CreateProject;

final class CreateProjectAction
{
    /**
     * @param CreateProjectResponder $responder
     * @return \Illuminate\Http\Response
     */
    public function __invoke(CreateProjectResponder $responder)
    {
        return $responder->getResponse();
    }
}
