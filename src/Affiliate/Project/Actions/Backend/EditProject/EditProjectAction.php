<?php
namespace Innovation\Affiliate\Project\Actions\Backend\EditProject;

use Innovation\Affiliate\Project\Contracts\ProjectInterface;

class EditProjectAction
{
    public function __invoke(ProjectInterface $project, EditProjectResponder $responder)
    {
        return $responder->getResponse($project);
    }
}
