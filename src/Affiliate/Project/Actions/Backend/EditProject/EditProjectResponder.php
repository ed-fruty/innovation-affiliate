<?php
namespace Innovation\Affiliate\Project\Actions\Backend\EditProject;

use Illuminate\Http\Response;
use Innovation\Affiliate\Common\Aware\Contracts\ViewFactoryAwareInterface;
use Innovation\Affiliate\Common\Aware\Traits\ViewFactoryAware;
use Innovation\Affiliate\Project\Contracts\ProjectInterface;

class EditProjectResponder implements ViewFactoryAwareInterface
{
    use ViewFactoryAware;

    private const VIEW_NAME = 'projects.backend.edit';

    /**
     * @param ProjectInterface $project
     * @return Response
     */
    public function getResponse(ProjectInterface $project)
    {
        return new Response(
            $this->viewFactory->make(self::VIEW_NAME, compact('project'))
        );
    }
}
