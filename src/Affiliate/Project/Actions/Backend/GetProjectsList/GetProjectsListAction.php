<?php
namespace Innovation\Affiliate\Project\Actions\Backend\GetProjectsList;

use Illuminate\Http\Request;
use Innovation\Affiliate\Project\Finders\ProjectListFinder;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GetProjectsListAction
 * @package Innovation\Affiliate\Project\Actions\Backend\GetProjectsList
 */
final class GetProjectsListAction
{
    /**
     * @var ProjectListFinder
     */
    private $finder;

    /**
     * GetProjectsListAction constructor.
     * @param ProjectListFinder $finder
     */
    public function __construct(ProjectListFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param Request $request
     * @param GetProjectsListResponder $responder
     * @return Response
     */
    public function __invoke(Request $request, GetProjectsListResponder $responder)
    {
        $projects = $this->finder->findPaginated();

        return $responder->getResponse($projects);
    }
}
