<?php
namespace Innovation\Affiliate\Project\Actions\Backend\GetProjectsList;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Innovation\Affiliate\Common\Aware\Contracts\HttpRequestAwareInterface;
use Innovation\Affiliate\Common\Aware\Contracts\ViewFactoryAwareInterface;
use Innovation\Affiliate\Common\Aware\Traits\HttpRequestAware;
use Innovation\Affiliate\Common\Aware\Traits\ViewFactoryAware;

class GetProjectsListResponder implements ViewFactoryAwareInterface, HttpRequestAwareInterface
{
    use ViewFactoryAware, HttpRequestAware;

    private const VIEW_NAME = 'projects.backend.index';

    /**
     * @param $projects
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getResponse($projects)
    {
        return $this->httpRequest->ajax()
            ?   new JsonResponse($projects)
            :   new Response($this->viewFactory->make(static::VIEW_NAME, compact('projects')));
    }
}
