<?php
namespace Innovation\Affiliate\Project\Actions\Backend\StoreProject;

use Innovation\Affiliate\Common\Aware\Contracts\CommandBusAwareInterface;
use Innovation\Affiliate\Common\Aware\Traits\CommandBusAware;
use Innovation\Affiliate\Project\Commands\StoreProjectCommand;

/**
 * Class StoreProjectAction
 * @package Innovation\Affiliate\Project\Actions\Backend\StoreProject
 */
class StoreProjectAction implements CommandBusAwareInterface
{
    use CommandBusAware;

    /**
     * @param StoreProjectRequest $request
     * @param StoreProjectResponder $responder
     * @return mixed
     */
    public function __invoke(StoreProjectRequest $request, StoreProjectResponder $responder)
    {
        $command = new StoreProjectCommand($request->get('name'));

        $project = $this->commandBus->dispatch($command);

        return $responder->getResponse($project);
    }
}
