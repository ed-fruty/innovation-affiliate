<?php
namespace Innovation\Affiliate\Project\Actions\Backend\StoreProject;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Innovation\Affiliate\Common\Aware\Contracts\HttpRequestAwareInterface;
use Innovation\Affiliate\Common\Aware\Contracts\RedirectAwareInterface;
use Innovation\Affiliate\Common\Aware\Traits\HttpRequestAware;
use Innovation\Affiliate\Common\Aware\Traits\RedirectAware;
use Innovation\Affiliate\Project\Contracts\ProjectInterface;

/**
 * Class StoreProjectResponder
 * @package Innovation\Affiliate\Project\Actions\Backend\StoreProject
 */
class StoreProjectResponder implements HttpRequestAwareInterface, RedirectAwareInterface
{
    use HttpRequestAware, RedirectAware;

    protected const REDIRECT_ROUTE = 'projects.backend.index';

    /**
     * @param ProjectInterface $project
     * @return RedirectResponse|JsonResponse
     */
    public function getResponse($project)
    {
        return $this->httpRequest->ajax()
            ?   new JsonResponse($project)
            :   $this->redirect->route(self::REDIRECT_ROUTE);
    }
}
