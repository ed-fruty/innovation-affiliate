<?php
namespace Innovation\Affiliate\Project\Actions\Backend\StoreProject;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreProjectRequest
 * @package Innovation\Affiliate\Project\Actions\Backend\StoreProject
 */
class StoreProjectRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|string'
        ];
    }
}
