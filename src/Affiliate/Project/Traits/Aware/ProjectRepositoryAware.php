<?php
namespace Innovation\Affiliate\Project\Traits\Aware;

use Innovation\Affiliate\Project\Contracts\ProjectRepositoryInterface;


/**
 * Trait ProjectRepositoryAware
 * @package Innovation\Affiliate\Project\Traits\Aware
 */
trait ProjectRepositoryAware
{
    /**
     * @var ProjectRepositoryInterface
     */
    protected $projectRepository;

    /**
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function setProjectRepository(ProjectRepositoryInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }
}