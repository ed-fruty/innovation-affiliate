<?php
namespace Innovation\Affiliate\Project\Handlers;

use Innovation\Affiliate\Common\Aware\Contracts\EventDispatcherAwareInterface;
use Innovation\Affiliate\Common\Aware\EventDispatcherAware;
use Innovation\Affiliate\Project\Commands\StoreProjectCommand;
use Innovation\Affiliate\Project\Events\ProjectWasCreated;
use Innovation\Affiliate\Project\Contracts\CommandProjectFactoryInterface;
use Innovation\Affiliate\Project\Contracts\ProjectInterface;
use Innovation\Affiliate\Project\Contracts\ProjectRepositoryAwareInterface;
use Innovation\Affiliate\Project\Traits\Aware\ProjectRepositoryAware;

/**
 * Class StoreProjectHandler
 * @package Innovation\Affiliate\Project\Handlers
 */
final class StoreProjectHandler implements EventDispatcherAwareInterface, ProjectRepositoryAwareInterface
{
    use EventDispatcherAware, ProjectRepositoryAware;

    /**
     * @param StoreProjectCommand $command
     * @return ProjectInterface
     */
    public function handle(StoreProjectCommand $command)
    {
        $project = $this->projectRepository->newInstance($command->toArray());

        $this->projectRepository->save($project);
        $this->eventDispatcher->dispatch(new ProjectWasCreated($project));

        return $project;
    }
}