<?php
namespace Innovation\Affiliate\Project\Handlers;

use Innovation\Affiliate\Common\Aware\EventDispatcherAware;
use Innovation\Affiliate\Project\Commands\UpdateProjectCommand;
use Innovation\Affiliate\Project\Traits\Aware\ProjectRepositoryAware;

class UpdateProjectHandler
{
    use ProjectRepositoryAware, EventDispatcherAware;

    /**
     * @param UpdateProjectCommand $command
     * @return \Innovation\Affiliate\Project\Contracts\ProjectInterface
     */
    public function handle(UpdateProjectCommand $command)
    {
        $project = $this->projectRepository->findById($command->getProjectId());

        $updatedProject = $this->projectRepository->newInstance([
            'id'    => $project->getId(),
            'name'  => $command->getNewName()
        ]);

        $this->projectRepository->save($updatedProject);

        $this->eventDispatcher->dispatch(new ProjectWasUpdated($updatedProject, [
            /*
             * Project old attributes
             */
            'name'  => $project->getName()
        ]));

        return $updatedProject;
    }
}
