<?php
namespace Innovation\Affiliate\Project\Finders;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Innovation\Affiliate\Common\Database\Specifcations\PaginatedSpecification;
use Innovation\Affiliate\Project\Contracts\ProjectRepositoryAwareInterface;
use Innovation\Affiliate\Project\Contracts\ProjectRepositoryInterface;
use Innovation\Affiliate\Project\Traits\Aware\ProjectRepositoryAware;

/**
 * Class ProjectListFinder
 * @package Innovation\Affiliate\Project\Finders
 */
class ProjectListFinder implements ProjectRepositoryAwareInterface
{
    use ProjectRepositoryAware;

    /**
     * @return Collection|LengthAwarePaginator|null
     */
    public function findPaginated()
    {
        return $this->projectRepository->match(new PaginatedSpecification());
    }
}
