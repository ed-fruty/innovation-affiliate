<?php
namespace Innovation\Affiliate\Common\Providers;

use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Support\ServiceProvider;
use Innovation\Affiliate\Common\Aware\Contracts\EventDispatcherAwareInterface as HasEvents;
use Innovation\Affiliate\Common\Aware\Contracts\HttpRequestAwareInterface as HasRequest;
use Innovation\Affiliate\Common\Aware\Contracts\RedirectAwareInterface as HasRedirect;
use Innovation\Affiliate\Common\Aware\Contracts\ViewFactoryAwareInterface as HasView;
use Innovation\Affiliate\Common\Aware\Contracts\CommandBusAwareInterface as HasCommandBus;

class AwareServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->afterResolving(HasEvents::class, function(HasEvents $aware) {
            $aware->setEventDispatcher($this->app['events']);
        });

        $this->app->afterResolving(HasRequest::class, function(HasRequest $aware) {
            $aware->setHttpRequest($this->app['request']);
        });

        $this->app->afterResolving(HasRedirect::class, function(HasRedirect $aware) {
            $aware->setRedirect($this->app['redirector']);
        });

        $this->app->afterResolving(HasView::class, function(HasView $aware) {
            $aware->setViewFactory($this->app['view']);
        });

        $this->app->afterResolving(HasCommandBus::class, function(HasCommandBus $aware) {
            $aware->setCommandBus($this->app[Dispatcher::class]);
        });
    }
}
