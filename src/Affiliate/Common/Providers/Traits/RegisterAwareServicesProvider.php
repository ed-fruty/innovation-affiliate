<?php
namespace Innovation\Affiliate\Common\Providers\Traits;

trait RegisterAwareServicesProvider
{
    public function registerAwareServices(array $aware)
    {
        foreach ($aware as $service => $values) {
            $this->{'app'}->afterResolving($service, function($concrete) use ($values) {

                foreach ($values as $method => $dependency) {
                    if (is_string($dependency)) {

                        $concrete->{$method}($this->{'app'}[$dependency]);

                    } elseif (is_array($dependency)) {
                        $arguments = [];

                        foreach ($dependency as $item) {
                            $arguments[] = $this->{'app'}[$item];
                        }

                        $concrete->{$method}(...$arguments);
                    }
                }
            });
        }
    }
}
