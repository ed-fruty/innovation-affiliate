<?php
namespace Innovation\Affiliate\Common\Providers\Traits;

use Illuminate\Contracts\Events\Dispatcher;

trait RegisterEventsProvider
{
    /**
     * Register event listeners.
     *
     * @param array $events
     */
    protected function registerEventListeners(array $events)
    {
        /** @var Dispatcher $event */
        $dispatcher = $this->getEventDispatcher();

        foreach ($events as $event => $listeners) {
            foreach ((array) $listeners as $listener) {
                $dispatcher->listen($event, $listener);
            }
        }
    }

    /**
     * Register event subscribers.
     *
     * @param array $subscribers
     */
    protected function registerEventSubscribers(array $subscribers)
    {
        $dispatcher = $this->getEventDispatcher();

        foreach ($subscribers as $subscriber) {
            $dispatcher->subscribe($subscriber);
        }
    }

    /**
     * @return Dispatcher
     */
    private function getEventDispatcher()
    {
        return $this->{'app'}['events'];
    }

}
