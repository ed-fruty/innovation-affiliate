<?php
namespace Innovation\Affiliate\Common\Providers\Traits;
use Illuminate\Contracts\Bus\Dispatcher;


/**
 * Trait RegisterCommandHandlersProvider
 * @package Innovation\Affiliate\Common\Providers\Traits
 */
trait RegisterCommandHandlersProvider
{
    /**
     * Register command bus handlers.
     *
     * @param array $commands
     */
    protected function registerCommandHandlers(array $commands)
    {
        $this->{'app'}->afterResolving(Dispatcher::class, function (Dispatcher $bus) use ($commands) {
             /** @var \Illuminate\Bus\Dispatcher $bus */
             $bus->map($commands);
        });
    }
}