<?php
namespace Innovation\Affiliate\Common\Database\Specifications;

class AggregateSpecification extends \SplQueue
{
    /**
     * AggregateSpecification constructor.
     * @param array ...$args
     */
    public function __construct(...$args)
    {
        foreach ($args as $specification) {
            $this->push($specification);
        }
    }

    public function apply($query)
    {

    }
}
