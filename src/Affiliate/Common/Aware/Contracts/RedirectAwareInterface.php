<?php
namespace Innovation\Affiliate\Common\Aware\Contracts;

use Illuminate\Routing\Redirector;

interface RedirectAwareInterface
{
    /**
     * @param Redirector $redirect
     */
    public function setRedirect(Redirector $redirect);
}
