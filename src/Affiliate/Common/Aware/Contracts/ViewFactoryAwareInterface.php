<?php
namespace Innovation\Affiliate\Common\Aware\Contracts;

use Illuminate\Contracts\View\Factory;

interface ViewFactoryAwareInterface
{
    /**
     * @param Factory $viewFactory
     */
    public function setViewFactory(Factory $viewFactory);
}
