<?php
namespace Innovation\Affiliate\Common\Aware\Contracts;

use Illuminate\Contracts\Events\Dispatcher;

/**
 * Interface EventDispatcherAwareInterface
 * @package Innovation\Affiliate\Common\Aware\Contracts
 */
interface EventDispatcherAwareInterface
{
    /**
     * @param Dispatcher $dispatcher
     * @return mixed
     */
    public function setEventDispatcher(Dispatcher $dispatcher);
}