<?php
namespace Innovation\Affiliate\Common\Aware\Contracts;

use Illuminate\Bus\Dispatcher;

/**
 * Interface CommandBusAwareInterface
 * @package Innovation\Affiliate\Common\Aware\Contracts
 */
interface CommandBusAwareInterface
{
    /**
     * @param Dispatcher $commandBus
     * @return mixed
     */
    public function setCommandBus(Dispatcher $commandBus);
}
