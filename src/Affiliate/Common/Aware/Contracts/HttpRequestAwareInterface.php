<?php
namespace Innovation\Affiliate\Common\Aware\Contracts;

use Illuminate\Http\Request;

/**
 * Interface HttpRequestAwareInterface
 * @package Innovation\Affiliate\Common\Aware\Contracts
 */
interface HttpRequestAwareInterface
{
    /**
     * Set Http request.
     *
     * @param Request $request
     */
    public function setHttpRequest(Request $request);
}
