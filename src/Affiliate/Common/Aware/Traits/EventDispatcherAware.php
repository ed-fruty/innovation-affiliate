<?php
namespace Innovation\Affiliate\Common\Aware;
use Illuminate\Contracts\Events\Dispatcher;


/**
 * Trait EventDispatcherAware
 * @package Innovation\Affiliate\Common\Aware
 */
trait EventDispatcherAware
{
    /**
     * @var Dispatcher
     */
    protected $eventDispatcher;

    /**
     * @param Dispatcher $dispatcher
     */
    public function setEventDispatcher(Dispatcher $dispatcher)
    {
        $this->eventDispatcher = $dispatcher;
    }
}