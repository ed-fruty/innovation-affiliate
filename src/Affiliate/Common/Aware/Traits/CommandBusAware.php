<?php
namespace Innovation\Affiliate\Common\Aware\Traits;

use Illuminate\Bus\Dispatcher;

trait CommandBusAware
{
    /**
     * @var Dispatcher
     */
    protected $commandBus;

    /**
     * @param Dispatcher $commandBus
     */
    public function setCommandBus(Dispatcher $commandBus)
    {
        $this->commandBus = $commandBus;
    }
}
