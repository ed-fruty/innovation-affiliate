<?php
namespace Innovation\Affiliate\Common\Aware\Traits;

use Illuminate\Routing\Redirector;

trait RedirectAware
{
    /**
     * @var Redirector
     */
    protected $redirect;

    /**
     * @param Redirector $redirect
     */
    public function setRedirect(Redirector $redirect)
    {
        $this->redirect = $redirect;
    }
}
