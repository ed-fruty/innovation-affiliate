<?php
namespace Innovation\Affiliate\Common\Aware\Traits;

use Illuminate\Http\Request;

/**
 * Class HttpRequestAware
 * @package Innovation\Affiliate\Common\Aware\Traits
 */
trait HttpRequestAware
{
    /**
     * @var Request
     */
    protected $httpRequest;

    /**
     * Set Http request.
     *
     * @param Request $request
     */
    public function setHttpRequest(Request $request)
    {
        $this->httpRequest = $request;
    }
}
