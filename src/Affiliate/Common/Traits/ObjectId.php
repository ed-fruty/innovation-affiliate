<?php
namespace Innovation\Affiliate\Common\Traits;


/**
 * Trait ObjectId
 * @package Innovation\Affiliate\Common\Traits
 */
trait ObjectId
{
    /**
     * @var string
     */
    private $id;

    /**
     * ObjectId constructor.
     * @param $id
     */
    final public function __construct($id)
    {
        $this->id = (string) $id;
    }

    /**
     * Get id value.
     *
     * @return string
     */
    final public function getId() : string
    {
        return $this->id;
    }

    /**
     * Converting object to string.
     *
     * @return string
     */
    final public function __toString() : string
    {
        return $this->getId();
    }

    /**
     * Check is id is equals with the given id.
     *
     * @param ObjectId $id
     * @return bool
     */
    final public function equals($id) : bool
    {
        if (false === ($id instanceof static)) {
            return false;
        }
        return $id->getId() === $this->getId();
    }

}