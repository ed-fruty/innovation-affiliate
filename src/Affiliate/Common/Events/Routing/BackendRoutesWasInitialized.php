<?php
namespace Innovation\Affiliate\Common\Events\Routing;

use Illuminate\Routing\Router;

/**
 * Class BackendRoutesWasInitialized
 * @package Innovation\Affiliate\Common\Events\Routing
 */
class BackendRoutesWasInitialized
{
    /**
     * @var Router
     */
    private $router;

    /**
     * BackendRoutesWasInitialized constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @return Router
     */
    public function getRouter(): Router
    {
        return $this->router;
    }
}
